import axios from "axios";
import store from "@/store";
import router from "@/router";

const serviceRequestResponse = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
});
serviceRequestResponse.interceptors.request.use(function (config) {
    const token = store.state.authModule.accessToken;
    if (token) {
        config.headers.common["Access-Control-Allow-Origin"] = "*";
        config.headers.common["Authorization"] = "Bearer " + token;
    }
    return config;
});
serviceRequestResponse.interceptors.response.use(
    function (response) {
        return response;
    },

    function (error) {
        if (
            error.response.status === 401 || error.response.status === 403
        ) {
            router.push({name: "login"})
        }
        return Promise.reject(error);
    }
);


export default serviceRequestResponse;