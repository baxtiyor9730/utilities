import serviceRequestResponse from "@/services";

function getOrganization(organizationPayload) {
    return serviceRequestResponse.get("/Organization", {
        params: {
            pageNumber: organizationPayload.pageNumber,
            pageSize: organizationPayload.pageSize
        }
    })
}

function createOrganization(organizationPayload) {
    return serviceRequestResponse.post("/Organization", {
        name: organizationPayload.name,
        regionId: organizationPayload.regionId,
        messageTitle: organizationPayload.messageTitle,
        contactNumber: organizationPayload.contactNumber,
        listRegionId: organizationPayload.listRegionId,
    })
}

function deleteOrganization(organizationPayload) {
    return serviceRequestResponse.delete("/Organization", {
        params: {
            id: organizationPayload.id
        }
    })
}

function updateOrganization(organizationPayload) {
    return serviceRequestResponse.put("/Organization", {
        name: organizationPayload.name,
        regionId: organizationPayload.regionId,
        listRegionId: organizationPayload.listRegionId,
        messageTitle: organizationPayload.messageTitle,
        contactNumber: organizationPayload.contactNumber,
        id: organizationPayload.id,
        parentId: organizationPayload.parentId,
    })
}

export default {
    getOrganization,
    createOrganization,
    deleteOrganization,
    updateOrganization
}