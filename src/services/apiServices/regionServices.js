import serviceRequestResponse from "@/services";


function createRegion(regionPayload) {
    return serviceRequestResponse.post("/region", {
        regionIndex: regionPayload.regionIndex,
        uzName: regionPayload.uzName
    })
}

function getRegions() {
    return serviceRequestResponse.get("/Region/regions")
}

function getRegionsByManager(managerPayload) {
    return serviceRequestResponse.get("Region/RegionsByManager", {
        params: {
            regionIndex: managerPayload.regionIndex,
            singleItemId: managerPayload.singleItemId,
            search: managerPayload.search,
            pageNumber: managerPayload.pageNumber,
            pageSize: managerPayload.pageSize,
            id: managerPayload.id
        }
    })
}

function deleteRegion(regionPayload) {
    return serviceRequestResponse.delete("/region", {
        params: {
            id: regionPayload.id,
        }
    })
}

function updateRegion(regionPayload) {
    return serviceRequestResponse.put("/region", {
        regionIndex: regionPayload.regionIndex,
        uzName: regionPayload.uzName,
        id: regionPayload.id,
    })
}

export default {
    getRegions,
    createRegion,
    getRegionsByManager,
    deleteRegion,
    updateRegion
}