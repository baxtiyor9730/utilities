import serviceRequestResponse from "@/services";
function createUser(userPayload) {
    return serviceRequestResponse.post("Manager/login", {
        loginDTO: userPayload.login,
        password:userPayload.password,
    });
}
function getPermission() {
    return serviceRequestResponse.get("Manager/permission")
}
export default {
    createUser,
    getPermission
}