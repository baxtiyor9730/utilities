import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import authModule from "@/store/modules/authModule";
import managerModule from "@/store/modules/managerModule";
import organizationModule from "@/store/modules/organizationModule";
import regionModule from "@/store/modules/regionModule";

const store = new Vuex.Store({
    modules: {
        authModule,
        managerModule,
        organizationModule,
        regionModule,
    },
});

export default store;