import serviceRequestResponse from "@/services";

function getManager(managerPayload) {
    return serviceRequestResponse.get("Manager/", {
        params: {
            pageNumber: managerPayload.pageNumber,
            pageSize: managerPayload.pageSize,
            id: managerPayload.id,
            singleItemId: managerPayload.singleItemId,
            search: managerPayload.search,
        }
    })
}

function createManager(managerPayload) {
    return serviceRequestResponse.post("Manager/Manager", {
        firstName: managerPayload.firstName,
        lastName: managerPayload.lastName,
        phoneNumber: managerPayload.phoneNumber,
        login: managerPayload.login,
        password: managerPayload.password,
        confirmPassword: managerPayload.confirmPassword,
        email: managerPayload.email,
        role: managerPayload.role,
        organizationId: managerPayload.organizationId,
        regionId: managerPayload.regionId,
        listRegionId: managerPayload.listRegionId,

    })
}

function updateManager(managerPayload) {
    return serviceRequestResponse.put("Manager", {
        id: managerPayload.id,
        firstName: managerPayload.firstName,
        lastName: managerPayload.lastName,
        phoneNumber: managerPayload.phoneNumber,
        login: managerPayload.login,
        password: managerPayload.password,
        confirmPassword: managerPayload.confirmPassword,
        email: managerPayload.email,
        role: managerPayload.role,
        organizationId: managerPayload.organizationId,
        regionId: managerPayload.regionId,
        listRegionId: managerPayload.listRegionId,
    })
}

function deleteManager(managerPayload) {
    return serviceRequestResponse.delete("Manager", {
        params: {
            id: managerPayload.id,
        }
    })
}

export default {
    getManager,
    createManager,
    deleteManager,
    updateManager,
}