import authServices from "@/services/apiServices/authServices";

export default {
    state: {
        accessToken: localStorage.getItem("accessToken"),
        userPermissions: [],
        loading: false,
    },
    mutations: {
        accessToken(state, accessToken) {
            state.accessToken = accessToken;
            if (accessToken) {
                localStorage.setItem("accessToken", accessToken);
            } else {
                localStorage.removeItem("accessToken");
            }
        },
        userPermissions(state, userPermissions) {
            state.userPermissions = userPermissions
        },
        loading(state, loading) {
            state.loading = loading
        }
    },
    actions: {
        createAuthToken({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                authServices
                    .createUser(authPayload)
                    .then((response) => {
                        if (response) {
                            commit("accessToken", response.data);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        getUserDetails({commit}) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                authServices
                    .getPermission()
                    .then((response) => {
                        if (response) {
                            commit("userPermissions", response.data.data.permissions);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },

    },
    getters: {
        hasPermission(state) {
            return (key) =>
                state.userPermissions ? state.userPermissions.includes(key) : false;
        },
    },
}