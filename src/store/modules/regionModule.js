import regionServices from "@/services/apiServices/regionServices";

export default {
    state: {
        regionList: [],
        regionByManagerList: [],
        regionTotalCount: null,
        loading: false,
    },
    mutations: {
        regionList(state, regionList) {
            state.regionList = regionList
        },

        regionByManagerList(state, regionByManagerList) {
            state.regionByManagerList = regionByManagerList
        },

        regionTotalCount(state, regionTotalCount) {
            state.regionTotalCount = regionTotalCount
        },
        loading(state, loading) {
            state.loading = loading
        }
    },
    actions: {
        getRegionsByManager({commit}, managerPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                regionServices
                    .getRegionsByManager(managerPayload)
                    .then((response) => {
                        if (response) {
                            commit("regionByManagerList", response.data.data);
                            commit("regionTotalCount", response.data.totalCount);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        getRegionsList({commit},) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                regionServices
                    .getRegions()
                    .then((response) => {
                        if (response) {
                            commit("regionList", response.data.data);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        createRegion({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                regionServices
                    .createRegion(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        deleteRegion({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                regionServices
                    .deleteRegion(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        updateRegion({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                regionServices
                    .updateRegion(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },

    }
}