import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/layouts/login/Login";
import Sidenav from "@/layouts/sidenav/Sidenav";
import IndexOrganization from "@/views/organization/IndexOrganization";
import IndexUsers from "@/views/users/IndexUsers";
import IndexRegions from "@/views/regions/IndexRegions";
import IndexMessages from "@/views/messages/IndexMessages";

Vue.use(VueRouter);
const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            redirect: "/login",
        },
        {
            name: "home",
            path: "/",
            redirect: "/messages",
            component: Sidenav,
            children: [
                {
                    name: "organization",
                    path: "/organization",
                    component: IndexOrganization,
                },
                {
                    name: "users",
                    path: "/users",
                    component: IndexUsers,
                },
                {
                    name: "regions",
                    path: "/regions",
                    component: IndexRegions,
                },
                {
                    name: "messages",
                    path: "/messages",
                    component: IndexMessages,
                },

            ],
        },
        {
            name: "login",
            path: "/login",
            component: Login

        },
        {
            path: '/:pathMatch(.*)*',
            redirect: '/login',
        },
    ]
})
export default router