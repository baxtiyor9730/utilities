import managerServices from "@/services/apiServices/managerServices";

export default {
    state: {
        managerList: [],
        managerCount:null,
        loading: false,
    },
    mutations: {
        managerList(state, managerList) {
            state.managerList = managerList
        },
        managerCount(state,managerCount){
            state.managerCount = managerCount
        },
        loading(state, loading) {
            state.loading = loading
        }
    },
    actions: {
        getManagerList({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                managerServices
                    .getManager(authPayload)
                    .then((response) => {
                        if (response) {
                            commit("managerList", response.data.data);
                            commit("managerCount", response.data.totalCount);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        createManager({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                managerServices
                    .createManager(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        updateManager({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                managerServices
                    .updateManager(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        deleteManager({commit}, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                managerServices
                    .deleteManager(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
    }
}