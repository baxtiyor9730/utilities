import organizationServices from "@/services/apiServices/organizationServices";

export default {
    state:{
        organizationList:[],
        organizationCount:null,
        loading:false,
    },
    mutations:{
        organizationList(state, organizationList){
            state.organizationList = organizationList
        },
        organizationCount(state, organizationCount){
            state.organizationCount = organizationCount
        },
        loading(state, loading){
            state.loading = loading
        }
    },
    actions:{
        getOrganizationList({ commit }, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                organizationServices
                    .getOrganization(authPayload)
                    .then((response) => {
                        if (response) {
                            commit("organizationList", response.data.data);
                            commit("organizationCount", response.data.totalCount);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch(() => {
                        reject();
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        createOrganization({ commit }, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                organizationServices
                    .createOrganization(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        updateOrganization({ commit }, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                organizationServices
                    .updateOrganization(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
        deleteOrganization({ commit }, authPayload) {
            commit("loading", true);
            return new Promise((resolve, reject) => {
                organizationServices
                    .deleteOrganization(authPayload)
                    .then((response) => {
                        if (response) {
                            resolve(response.data);
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error.data);
                    })
                    .finally(() => {
                        commit("loading", false);
                    });
            });
        },
    }
}