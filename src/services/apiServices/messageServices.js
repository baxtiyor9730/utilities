import serviceRequestResponse from "@/services";

function sendMessage(messagePayload) {
    return serviceRequestResponse.post(`/Message?language=${messagePayload.language}`, {
        title: messagePayload.title,
        content: messagePayload.content,
        regionId: messagePayload.regionId,
        managerId: messagePayload.managerId,
        listRegionId: messagePayload.listRegionId
    })
}

export default {
    sendMessage
}